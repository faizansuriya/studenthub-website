import React, { Component } from "react";

export default class CommentInput extends Component {
  state = {
    text: "" // user's input
  };

  // Update state when input changes
  onChangeText = text => {
    console.log("hello==>", text.target.value);
    this.setState({ text: text.target.value });
  };

  // Handle return press on the keyboard
  // NOTE: You don't really need it for this example, because
  // we're using a keyboard without return button, but I left it here
  // in case you'd want to switch to a different keyboard
  //onSubmitEditing = ({ text }) => this.setState({ text }, this.submit);

  // Call this.props.onSubmit handler and pass the comment
  submit = () => {
    const { text } = this.state;
    if (text) {
      this.props.onSubmit(text);
    } else {
      alert("Please enter your comment first");
    }
  };

  render() {
    return (
      <section className="box comment-post">
        <div className="field">
          <div className="control">
            <input
              className="input is-normal"
              placeholder="Write your comment..."
              onChange={this.onChangeText}
            />
            <a onClick={() => this.submit()} className="button post">
              Post
            </a>
          </div>
        </div>
      </section>
      //   // This moves children view with input field and submit button
      //   // up above the keyboard when it's active
      //   <KeyboardAvoidingView behavior="position">
      //     <View style={styles.container}>
      //       {/* Comment input field */}
      //       <TextInput
      //         placeholder="Add a comment..."
      //         keyboardType="twitter" // keyboard with no return button
      //         autoFocus={false} // focus and show the keyboard
      //         style={styles.input}
      //         value={this.state.text}
      //         onChangeText={this.onChangeText} // handle input changes
      //         onSubmitEditing={this.onSubmitEditing} // handle submit event
      //       />
      //       {/* Post button */}
      //       <TouchableOpacity style={styles.button} onPress={this.submit}>
      //         {/* Apply inactive style if no input */}
      //         <Text
      //           style={[styles.text, !this.state.text ? styles.inactive : []]}
      //         >
      //           Post
      //         </Text>
      //       </TouchableOpacity>
      //     </View>
      //   </KeyboardAvoidingView>
    );
  }
}
