import React, { Component } from "react";
//import moment from "react-moment";
import moment from "moment";
import "./style.css";

export default class PostComment extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    // Pull comment object out of props
    const { comment } = this.props;
    console.log("This is comment page shows", comment);
    // Pull data needed to display a comment out of comment object
    // const { content, created, user } = comment;
    // Pull user name and avatar out of user object
    // const { name, avatar } = user;
    return (
      <section className="box comment">
        <p className="comment-text">{comment.text}</p>
        <p className="comment-time">{moment(comment.date).fromNow()}</p>
      </section>

      //   <View style={styles.container}>
      //     <View style={styles.avatarContainer}>
      //       {avatar && (
      //         <Image
      //           resizeMode="contain"
      //           style={styles.avatar}
      //           source={{ uri: avatar }}
      //         />
      //       )}
      //     </View>
      //     <View style={styles.contentContainer}>
      //       <Text>
      //         <Text style={[styles.text, styles.name]}>{name}</Text>

      //         <Text style={styles.text}>{comment.text}</Text>
      //       </Text>
      //       <Text style={[styles.text, styles.created]}>
      //         {moment(comment.date).fromNow()}
      //       </Text>
      //     </View>
      //   </View>
    );
  }
}
