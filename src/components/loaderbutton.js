import React, { Component } from "react";

export default props => {
  return (
    <button
      onClick={event => {
        props.onClick(event);
      }}
      className={`button is-block is-info is-large is-fullwidth ${props.loading &&
        "is-loading"}`}
    >
      {props.label}
    </button>
  );
};
