import React, {Component} from 'react';



export default class ImageUpload extends Component {
  constructor(props) {
    super(props);
    this.state = {
      file: '',
      imagePreviewUrl: ''
    };
    this._handleImageChange = this._handleImageChange.bind(this);
    this._handleSubmit = this._handleSubmit.bind(this);
  }

  _handleSubmit(e) {
    e.preventDefault();
    // TODO: do something with -> this.state.file
  }

  _handleImageChange(e) {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];

    const imageLocalUrl = URL.createObjectURL(file);
    this.props.onImageSelect(file);


    
    this.setState({
      file: file,
      imagePreviewUrl: imageLocalUrl
    });
            // reader.onloadend = () => {
    //   // this.props.onImageSelect(reader.result);
    // }

    // reader.readAsDataURL(file)
  }

  render() {
    let {imagePreviewUrl} = this.state;
    let $imagePreview = null;
    if (imagePreviewUrl) {
      $imagePreview = (<figure className="image image is-4by3">
      <img src={imagePreviewUrl} />
    </figure>);
    }

    return (
      <div>
        <form onSubmit={this._handleSubmit}>
          <input type="file" onChange={this._handleImageChange} />
          {/* <button type="submit" onClick={this._handleSubmit}>Upload Image</button> */}
        </form>
        {$imagePreview}
      </div>
    )
  }

}