import React, { Component } from 'react';

const saveButtonStyle = {
  delete: 'is-danger',
  save: 'is-primary',
};

class Modal extends Component {
  componentWillMount() {
    document.addEventListener('keydown', this._onKeyPressed.bind(this));
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this._onKeyPressed.bind(this));
  }

  _onKeyPressed = (e) => {
    if (e.keyCode === 27) {
      this._onClose();
    }
  };

  _onClose = () => {
    if (this.props.handleClose) {
      this.props.handleClose();
    }
  };

  render() {
    const {
      show, title, classname, overflow, saveButtonText,
    } = this.props;

    const btnCx = saveButtonText
      ? saveButtonStyle[saveButtonText.toLowerCase()]
      : saveButtonStyle.save;

    return (
      <div className={show ? 'modal is-active' : 'modal'} style={{ zIndex: 99999 }}>
        <div className="modal-background" />
        <div className={`modal-card ${classname} ${overflow && 'oflow-visible'}`}>
          <header className="modal-card-head">
            <p className="modal-card-title">{title}</p>
            <button onClick={this._onClose} className="delete is-danger" aria-label="close" />
          </header>
          <section className={`modal-card-body ${overflow && 'oflow-visible'}`}>
            {this.props.children}
          </section>
          {/* <footer className="modal-card-foot flex flex-end">
            {!this.props.isOff && (
              <button className="button is-light is-pulled-right" onClick={this._onClose}>
                Cancel
              </button>
            )}
            {!this.props.isOff ? (
              <button
                className={`button ${btnCx} is-pulled-right`}
                disabled={this.props.isDisabled}
                onClick={this.props.onSave}
              >
                {saveButtonText || 'Save'}
              </button>
            ) : (
              <div className="flex flex-space-around" style={{ width: '100%' }}>
                <button className="button is-info" onClick={this._onClose}>
                  Close
                </button>
              </div>
            )}
          </footer> */}
        </div>
      </div>
    );
  }
}

export default Modal;
