import React, { Component } from "react";
import { Link } from "react-router-dom";
import services from "../services";

export default class CategoryList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categories: []
    };
  }

  componentDidMount() {
    this.categories();
  }
  

  categories = async () => {
    try {
      const getData = await services.auth.getCategories();
      console.log("This is the Categories>>>", getData);
      this.setState({
        categories: getData.data,
        selectedCategory: getData.data[0]
      });
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    return (
      <div className="navbar-dropdown">
        {this.state.categories.map((item, i) => (
          <Link to={`/category/${item.name}/${item._id}`} className="navbar-item" key={i} >
            {item.name}
          </Link>
        ))}
      </div>
    );
  }
}
