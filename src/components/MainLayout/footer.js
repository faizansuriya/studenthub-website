import React from "react";
import "reactbulma";

class Footer extends React.Component {
  render() {
    return (
      <footer className="footer">
        <div className="content has-text-centered">
          <p>&copy; Copyright 2018, All Rights Reserved</p>
        </div>
      </footer>
    );
  }
}

export default Footer;
