import React from "react";
import Logo from "../../images/sh_logo.png";
import "./style.css";
import { Link, withRouter } from "react-router-dom";
import CategoryList from "../menucategorylist";
import * as authUtil from "../../utils/auth.util";
import { fakeAuthCentralState } from "../../Routes";

/* eslint-disable react/prefer-stateless-function */
class Header extends React.Component {
  render() {
    return (
      <nav className="navbar" role="navigation" aria-label="main navigation">
        <div className="navbar-brand">
          <Link to="/" className="navbar-item">
            <img src={Logo} width="150" height="200" />
          </Link>
          <Link
            to="/"
            role="button"
            className="navbar-burger"
            aria-label="menu"
            aria-expanded="true"
            data-target="navbarBasicExample"
          >
            <span aria-hidden="true" />
            <span aria-hidden="true" />
            <span aria-hidden="true" />
          </Link>
        </div>
        <div id="navbarBasicExample" className="navbar-menu">
          <div className="navbar-start">
            <Link to="/" className="navbar-item ">
              Home
            </Link>
            <Link to="/premium" className="navbar-item">
              Premium
            </Link>
            <div className="navbar-item has-dropdown is-hoverable">
              <Link to="#" className="navbar-link">
                Categories
              </Link>
              <CategoryList />
              {/* <div className="navbar-dropdown">
                <categoryList data={this.state.data} /> ==> data.map((item)=> (
                <Link to="/" className="navbar-item">
                  {item.name}
                </Link>
                ))
              </div> */}
            </div>

            <Link to="/aboutus" className="navbar-item">
              About Us
            </Link>
            <Link to="/blogs" className="navbar-item">
              Student Corner
            </Link>
            <Link to="/contact" className="navbar-item">
              Contact
            </Link>
          </div>
        </div>
        {authUtil.isLoggedIn() ? (
          <div className="navbar-end">
            <div className="navbar-item">
              <div className="buttons">
                <button
                  className="button is-primary"
                  onClick={() => {
                    authUtil.logout();
                    this.props.history.push("/login");
                  }}
                >
                  <strong>Logout</strong>
                </button>
              </div>
            </div>
          </div>
        ) : (
          <div className="navbar-end">
            <div className="navbar-item">
              <div className="buttons">
                <Link className="button is-primary" to="/signup">
                  <strong>Sign up</strong>
                </Link>
                <Link className="button is-light" to="/login">
                  Log in
                </Link>
              </div>
            </div>
          </div>
        )}
      </nav>
    );
  }
}

export default withRouter(Header);
