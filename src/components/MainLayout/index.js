import React from "react";
import { FormattedMessage } from "react-intl";

import { Layout, Navigation, Drawer, Content } from "react-mdl";
import { Link } from "react-router-dom";

import Header from "./header";
import Footer from "./footer";

/* eslint-disable react/prefer-stateless-function */
class MainLayout extends React.Component {
  render() {
    return (
      <div>
        <Header />
        {this.props.children}
        <Footer />
      </div>
    );
  }
}

export default MainLayout;
