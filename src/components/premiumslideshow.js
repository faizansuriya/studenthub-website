import React, { Component } from "react";
import ReactDOM from "react-dom";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from "react-responsive-carousel";
import services from "../services";

class PremiumSlideshow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      premium: []
    };
  }

  componentDidMount() {
    this.getPremiumVendors();
  }

  getPremiumVendors = async () => {
    try {
      const getData = await services.auth.getPremiumVendors();
      console.log("This is the slideshow Premium Vendors>>>", getData);
      this.setState({ premium: getData.data });
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    return (
      <Carousel
        autoPlay
        interval={1000}
        infiniteLoop
        showThumbs={false}
        showIndicators={false}
        showStatus={false}
        swipeable={true}
      >
        {this.state.premium.map((item, index) => (
          <div>
            <img src={item.picture} key={item._id} />
            <p className="legend">{item.name}</p>
          </div>
        ))}
      </Carousel>
    );
  }
}

export default PremiumSlideshow;
