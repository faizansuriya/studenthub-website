import React, { Component } from "react";

import Step1 from "./createaccount";
import Step2 from "./verification";
import Step3 from "./instituteinfo";

class Main extends Component {
  state = {
    activeIndex: 0
  };

  _moveOnNextStep = () => {
    this.setState(ps => {
      return { activeIndex: ps.activeIndex + 1 };
    });
  };

  //   _moveOnPrevStep = () => {
  //     this.setState(ps => {
  //       return { activeIndex: ps.activeIndex - 1 };
  //     });
  //   };

  render() {
    const { activeIndex } = this.state;

    const cmpList = [
      <Step1 moveOnNextStep={this._moveOnNextStep} />,
      <Step2 moveOnNextStep={this._moveOnNextStep} />,
      <Step3 />
    ];
    // const steps = [
    //   { name: "Step 1", component: <Step1 /> },
    //   { name: "Step 2", component: <Step2 /> },
    //   { name: "Step 3", component: <Step3 /> }
    // ];
    const activeCmp = cmpList[activeIndex];
    return <div className="step-progress">{activeCmp}</div>;
  }
}

export default Main;
