import React, { Component } from "react";
import Autocomplete from "../Autocomplete/Autocomplete";
import ImageUpload from "../Imageuploader";
import "../Autocomplete/style.css";
import _ from "lodash";
import services from "../../services";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Button from "../../components/loaderbutton";
import { String } from "../../utils/strings";

class Step3 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      institute: null,
      front: null,
      back: null,
      studyYear: "default",
      courseLength: "default",
      loading: false,
      names: [
        "Sir Syed University of Engineering & Technology",
        "NED University of Engineering & Technology",
        "Iqra University"
      ]
    };
  }

  _onAutoCompletedSelect = val => {
    console.log(val);
    this.setState({ institute: val });
  };

  _onFrontImageSelect = file => {
    console.log("front", file);
    this.setState({ front: file });
  };

  _onBackImageSelect = file => {
    console.log("Back", file);
    this.setState({ back: file });
  };

  _onhandleSubmit = async event => {
    if (_.map(this.state.names).indexOf(this.state.institute) < 0) {
      toast.error("Please select institute from list", {
        position: "bottom-center",
        autoClose: 2000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
    } else if (this.state.studyYear === "default") {
      toast.error("Please Select Study Year", {
        position: "bottom-center",
        autoClose: 2000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
    } else if (this.state.courseLength === "default") {
      toast.error("Please Select Course Lenght", {
        position: "bottom-center",
        autoClose: 2000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
    }

    try {
      this.setState({ loading: true });
      const formData = new FormData();
      formData.append("front", this.state.front);
      formData.append("back", this.state.back);
      formData.append("studyYear", this.state.studyYear);
      formData.append("courseLength", this.state.courseLength);
      formData.append("institute", this.state.institute);

      const payload = { ...this.state };
      console.log("hecho===>", payload);
      console.log("formData", formData);
      const { data } = await services.auth.updateInstitute(formData);
      this.setState({ loading: false });
      toast.success("Signup Successfull", {
        position: "bottom-center",
        autoClose: 2000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
    } catch (error) {
      this.setState({ loading: false });
      toast.error(error.response.data.message, {
        position: "bottom-center",
        autoClose: 2000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
      console.log(error.response.data.message);
    }
  };

  render() {
    return (
      <section className="hero is-success is-fullheight">
        <div className="hero-body">
          <div className="container has-text-centered">
            <div className="column is-8  is-offset-2">
              <h3 className="title has-text-grey">Almost Ready</h3>
              <p className="subtitle has-text-grey">
                Please Provide your educational information
              </p>

              <div className="box">
                <div className="field">
                  <div className="control">
                    <Autocomplete
                      suggestions={this.state.names}
                      onSelect={this._onAutoCompletedSelect}
                    />
                  </div>
                </div>
                <div className="columns">
                  <div className="column">
                    <div className="field">
                      <div className="control">
                        <div className="select is-large">
                          <select
                            onChange={event => {
                              console.log(event.target.value);
                              this.setState({ studyYear: event.target.value });
                            }}
                          >
                            <option value="default">Year Of Study</option>
                            <option value="2014">2014</option>
                            <option value="2015">2015</option>
                            <option value="2016">2016</option>
                            <option value="2017">2017</option>
                            <option value="2018">2018</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="column">
                    <div className="field">
                      <div className="control">
                        <div className="select is-large">
                          <select
                            onChange={event => {
                              console.log(event.target.value);
                              this.setState({
                                courseLength: event.target.value
                              });
                            }}
                          >
                            <option value="default">Course Lenght</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <h4 className="subtitle is-4 has-text-black">
                  <b>Upload Id Card Image</b>
                </h4>
                <div className="columns">
                  <div className="column">
                    <p className="subtitle has-text-grey">Front Image</p>
                    <ImageUpload onImageSelect={this._onFrontImageSelect} />
                  </div>
                  <div className="column">
                    <p className="subtitle has-text-grey">Back Image</p>
                    <ImageUpload onImageSelect={this._onBackImageSelect} />
                  </div>
                </div>

                <Button
                  loading={this.state.loading}
                  label={String.submit}
                  onClick={event => {
                    this._onhandleSubmit(event);
                  }}
                />
                <ToastContainer
                  position="top-center"
                  autoClose={2000}
                  hideProgressBar
                  newestOnTop={false}
                  closeOnClick
                  rtl={false}
                  pauseOnVisibilityChange
                  draggable
                  pauseOnHover
                />
                {/* Same as */}
                <ToastContainer />
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Step3;
