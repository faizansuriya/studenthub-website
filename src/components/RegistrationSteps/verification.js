import React, { Component } from "react";
import services from "../../services";
import { verifyConstraint } from "../../utils/constraints";
import Button from "../../components/loaderbutton";
import { String } from "../../utils/strings";

import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

var validate = require("validate.js");

class Step2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      smsCode: "",
      loading: false
    };
  }

  _handleVerify = async event => {
    try {
      this.setState({ loading: true });
      const { data } = await services.auth.verifyCode(this.state);
      this.setState({ loading: false });
      this.props.moveOnNextStep();
    } catch (error) {
      this.setState({ loading: false });
      toast.error(error.response.data.message, {
        position: "bottom-center",
        autoClose: 2000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
      console.log(error.response);
    }
  };

  render() {
    return (
      <section className="hero is-success is-fullheight">
        <div className="hero-body">
          <div className="container has-text-centered">
            <div className="column is-4 is-offset-4">
              <h3 className="title has-text-grey">Verify Now</h3>
              <p className="subtitle has-text-grey">
                Enter the verification code sent on your registered number
              </p>
              <div className="box">
                <div className="field">
                  <div className="control">
                    <input
                      onChange={event => {
                        this.setState({ smsCode: event.target.value });
                      }}
                      className="input is-large"
                      type="text"
                      placeholder={String.verify}
                    />
                  </div>
                </div>

                <Button
                  loading={this.state.loading}
                  label={String.submit}
                  onClick={event => {
                    this._handleVerify(event);
                  }}
                />
                <ToastContainer
                  position="bottom-center"
                  autoClose={2000}
                  hideProgressBar
                  newestOnTop={false}
                  closeOnClick
                  rtl={false}
                  pauseOnVisibilityChange
                  draggable
                  pauseOnHover
                />
                {/* Same as */}
                <ToastContainer />
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Step2;
