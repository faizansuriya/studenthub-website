import React, { Component } from "react";
import services, { updateHeaders } from "../../services";
import * as authUtil from "../../utils/auth.util";
import {
  nameConstraint,
  emailConstraint,
  phoneConstraint,
  passwordConstraint
} from "../../utils/constraints";
import { toast, ToastContainer } from "react-toastify";
import Button from "../../components/loaderbutton";
import { String } from "../../utils/strings";
import "react-toastify/dist/ReactToastify.css";

var validate = require("validate.js");

class Step1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: "",
      lastName: "",
      email: "",
      mobile: "",
      password: "",
      confirmPassword: "",
      loading: false
    };
  }

  _handleSignup = async event => {
    var constraints = {
      name: nameConstraint,
      email: emailConstraint,
      phone: phoneConstraint,
      password: passwordConstraint
    };
    var validation = validate(
      {
        name: this.state.firstName,
        name: this.state.lastName,
        email: this.state.email,
        phone: this.state.mobile,
        password: this.state.password
      },
      constraints,
      { format: "flat" }
    );
    //   console.log(validation);
    if (validation) {
      toast.error(validation[0], {
        position: "bottom-center",
        autoClose: 2000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
      return;
    }
    try {
      if (this.state.password !== this.state.confirmPassword) {
        toast.error("Password do not match", {
          position: "bottom-center",
          autoClose: 2000,
          hideProgressBar: true,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
        return;
      }
      this.setState({ loading: true });
      const { data } = await services.auth.registerUser(this.state);
      console.log("Sigup Successfull", data.token);
      updateHeaders(data.token);
      await authUtil.setToken(data.token);
      this.setState({ loading: false });
      this.props.moveOnNextStep();
    } catch (error) {
      this.setState({ loading: false });
      toast.error(error.response.data.message, {
        position: "bottom-center",
        autoClose: 2000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
      console.log(error.response);
    }
  };

  render() {
    return (
      <section className="hero is-success is-fullheight">
        <div className="hero-body">
          <div className="container has-text-centered">
            <div className="column is-4 is-offset-4">
              <h3 className="title has-text-grey">Create Account</h3>
              <p className="subtitle has-text-grey">Signup to continue</p>
              <div className="box">
                <div className="field">
                  <div className="control">
                    <input
                      onChange={event => {
                        this.setState({ firstName: event.target.value });
                      }}
                      className="input is-large"
                      type="name"
                      placeholder={String.firstName}
                    />
                  </div>
                </div>
                <div className="field">
                  <div className="control">
                    <input
                      onChange={event => {
                        this.setState({ lastName: event.target.value });
                      }}
                      className="input is-large"
                      type="name"
                      placeholder={String.lastName}
                    />
                  </div>
                </div>
                <div className="field">
                  <div className="control">
                    <input
                      onChange={event => {
                        this.setState({ email: event.target.value });
                      }}
                      className="input is-large"
                      type="email"
                      placeholder={String.email}
                    />
                  </div>
                </div>
                <div className="field">
                  <div className="control">
                    <input
                      onChange={event => {
                        this.setState({ mobile: event.target.value });
                      }}
                      className="input is-large"
                      type="phone"
                      placeholder={String.phoneNumber}
                    />
                  </div>
                </div>
                <div className="field">
                  <div className="control">
                    <input
                      onChange={event => {
                        this.setState({ password: event.target.value });
                      }}
                      className="input is-large"
                      type="password"
                      placeholder={String.password}
                    />
                  </div>
                </div>
                <div className="field">
                  <div className="control">
                    <input
                      onChange={event => {
                        this.setState({ confirmPassword: event.target.value });
                      }}
                      className="input is-large"
                      type="password"
                      placeholder={String.confirmPassword}
                    />
                  </div>
                </div>
                <Button
                  loading={this.state.loading}
                  label = {String.signUp}
                  onClick={event => {
                    this._handleSignup(event);
                  }}
                />
                <ToastContainer
                  position="bottom-center"
                  autoClose={2000}
                  hideProgressBar
                  newestOnTop={false}
                  closeOnClick
                  rtl={false}
                  pauseOnVisibilityChange
                  draggable
                  pauseOnHover
                />
                {/* Same as */}
                <ToastContainer />
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Step1;
