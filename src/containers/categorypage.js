import React, { Component } from "react";
import services from "../services";
import Modal from "../components/Modal";
import Button from "../components/loaderbutton";
import { String } from "../utils/strings";
import * as authUtil from "../utils/auth.util";
import Link from "reactbulma/lib/components/Link/Link";
export default class CategoryPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      vendors: [],
      selectedDeals: "",
      isOpen: false,
      code: ""
    };
  }

  componentDidMount() {
    const loginStatus = authUtil.isLoggedIn();
    console.log("This is category page login status>>>", loginStatus);
    this.getVendorsByCategory(this.props.match.params.id);
  }

  componentWillReceiveProps(newProps) {
    console.log("Component WILL RECIEVE PROPS!", newProps.match.params.id);
    if (this.props.match.params.id !== newProps.match.params.id) {
      this.getVendorsByCategory(newProps.match.params.id);
    }
  }

  _revealCodePressed = async event => {
    try {
      const getData = await services.auth.revealCode({
        vendor: this.state.selectedDeals._id
      });
      console.log("This is the Reveal Code Function>>>", getData);
      this.setState({ code: getData.data });
    } catch (error) {
      console.log(error);
    }
  };

  async getVendorsByCategory(id) {
    try {
      const getData = await services.auth.getVendorsByCategory(id);
      console.log("This is A getVendorsByCATEGORY", getData.data);
      this.setState({ vendors: getData.data });
    } catch (error) {
      console.log(error);
    }
  }

  _handleModalClose = () => {
    this.setState({ isOpen: false });
    this.setState({ code: null });
  };

  render() {
    return (
      <div>
        <section className="hero is-info is-small">
          <div className="hero-body">
            <div className="container has-text-centered">
              <p className="title">{this.props.match.params.name}</p>
              {/* <p className="subtitle">Subtitle</p> */}
            </div>
          </div>
        </section>
        <section className="container vendor ">
          <div className="columns features">
            <div class="columns is-multiline">
              {this.state.vendors.map((item, i) => (
                <div className="column is-3 deals">
                  <div className="card is-shady">
                    <div className="card-image">
                      <figure className="image is-4by4 is-260x136 deals">
                        <img
                          src={item.picture}
                          alt="Placeholder image"
                          className="modal-button"
                          data-target="modal-image2"
                        />
                      </figure>
                    </div>
                    <div className="card-content deals">
                      <div className="content deals has-text-centered">
                        <span className="tag is-info discount">
                          {item.discount} % off
                        </span>
                        <h4>{item.name}</h4>
                        <button
                          className="button is-link modal-button revealcode"
                          data-target="modal"
                          onClick={() => {
                            this.setState(
                              { isOpen: true, selectedDeals: item },
                              () =>
                                console.log(
                                  "This is the selected item",
                                  this.state.selectedDeals
                                )
                            );
                          }}
                        >
                          Reveal Code Now!
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
          <Modal
            show={this.state.isOpen}
            handleClose={this._handleModalClose}
            onSave={this._handleFormSubmit}
            title={"Reveal Code"}
          >
            <div className="columns is-multiline">
              <div className="column is-6 deals">
                <div className="content deals-modal">
                  <h4>{this.state.selectedDeals.name}</h4>
                  <p>Vaild from 12/8/2018 to 10/9/2018</p>
                  <h4>{this.state.selectedDeals.discount}% Off</h4>
                  <p>Reveal Your code and show it at the store</p>
                  {authUtil.isLoggedIn() ? (
                    <Button
                      loading={this.state.loading}
                      label={
                        this.state.code
                          ? this.state.code.code
                          : String.revealCode
                      }
                      onClick={event => {
                        this._revealCodePressed(event);
                      }}
                    />
                  ) : (
                    <Button
                      loading={this.state.loading}
                      label={"Continue to login"}
                      onClick={() => {
                        this.props.history.push({
                          pathname: "/login",
                          state: { from: this.props.history.location }
                        });
                      }}
                    />
                  )}
                </div>
              </div>
              <div className="column is-6 deals">
                <div className="card is-shady">
                  <div className="card-image">
                    <figure className="image is-4by4 is-260x136 deals">
                      <img
                        src={this.state.selectedDeals.picture}
                        alt="Placeholder image"
                        className="modal-button"
                        data-target="modal-image2"
                      />
                    </figure>
                  </div>
                </div>
              </div>
            </div>
          </Modal>
        </section>
      </div>
    );
  }
}
