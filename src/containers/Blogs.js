import React, { Component } from "react";
import services from "../services";
import { Link } from "react-router-dom";
import { String } from "../utils/strings";

class Blogs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: []
    };
  }

  componentDidMount() {
    this._getAllPost();
  }

  _getAllPost = async () => {
    try {
      const getData = await services.auth.getAllPost();
      console.log("This is the all posts", getData);
      this.setState({
        data: getData.data.data,
        selectedCategory: getData.data[0]
      });
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    return (
      <section>
        <section className="hero is-info is-small">
          <div className="hero-body">
            <div className="container has-text-centered">
              <p className="title">Student Corner</p>
            </div>
          </div>
        </section>
        <section className="container vendor">
          <div className="columns features">
            <div class="columns is-multiline">
              {this.state.data.map((item, i) => (
                <div className="column is-4">
                  <div className="card is-shady">
                    <div className="card-image">
                      <figure className="image is-4by3">
                        <img
                          src={item.media}
                          alt="Placeholder image"
                          className="modal-button"
                          data-target="modal-image2"
                        />
                      </figure>
                    </div>
                    <div className="columns is-full status">
                      <div className="column">
                        <i className="icon is-large status far fa-thumbs-up" />
                        {item.totalLikes}
                        <i className="icon is-large status far fa-comment" />
                        {item.totalComments}
                      </div>
                    </div>
                    <div className="card-content corner">
                      <div className="content blog">
                        <h4>{item.title}</h4>
                        {item.text.length > 60 ? (
                          <p>
                            {item.text.substr(0, 100)}
                            {"..."}
                          </p>
                        ) : (
                          <p>{item.text}</p>
                        )}

                        <Link
                          to={`blog/${item.title}/${item._id}`}
                          className="button is-link modal-button"
                          key={i}
                        >
                          {String.readMore}
                        </Link>
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </section>
      </section>
    );
  }
}

export default Blogs;
