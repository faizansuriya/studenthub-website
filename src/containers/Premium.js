import React, { Component } from "react";

class Premium extends Component {
  render() {
    return (
      <section className="hero is-info is-small">
        <div className="hero-body">
          <div className="container has-text-centered">
            <p className="title">Premium</p>
            {/* <p className="subtitle">Subtitle</p> */}
          </div>
        </div>
      </section>
    );
  }
}

export default Premium;
