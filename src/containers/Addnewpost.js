import React, { Component } from "react";
import services from "../services";
import ImageUpload from "../components/Imageuploader";
import Button from "../components/loaderbutton";
import { String } from "../utils/strings";

class AddNewPost extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      text: "",
      media: "",
      image: ""
    };
  }

  _onPostImageSelect = file => {
    console.log("media", file);
    this.setState({ image: file });
  };

  _updateImage = async () => {
    try {
      const formData = new FormData();
      formData.append(
        "file",
        this.state.image,
        `${new Date().getTime()}`,
        "image/jpeg"
      );

      console.log("This is the form Data", formData);
      const { data } = await services.auth.getImageUrl(formData);
      console.log("This is the upload path", data.path);
      this._postSubmit(data.path);
      // this.setState({ data: getData.data, selectedCategory: getData.data[0] });
    } catch (error) {
      console.log(error);
    }
  };

  _postSubmit = async path => {
    const payload = {
      media: path,
      title: this.state.title,
      text: this.state.text,
      thumb: path,
      mediaType: "image"
    };
    console.log("This is the Payload data", payload);
    try {
      const getData = await services.auth.addPost(payload);
      console.log("This is the payload data", getData);
      this.props.history.push("/blogs")
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    return (
      <section>
        <section className="hero is-info is-small">
          <div className="hero-body">
            <div className="container has-text-centered">
              <p className="title">Add New Forum</p>
            </div>
          </div>
        </section>
        <section className="hero is-success is-fullheight">
          <div className="hero-body">
            <div className="container">
              <div className="column is-8  is-offset-2">
                <div className="box">
                  <div className="field">
                    <div className="control">
                      <input
                        onChange={event => {
                          this.setState({ title: event.target.value });
                        }}
                        className="input is-large"
                        type="text"
                        placeholder={String.addForumTitle}
                      />
                    </div>
                  </div>
                  <div className="field">
                    <div className="control">
                      <textarea
                        className="textarea"
                        onChange={event => {
                          this.setState({ text: event.target.value });
                        }}
                        placeholder={String.addForumDescription}
                      />
                    </div>
                  </div>
                  <div className="columns">
                    <div className="column">
                      <p className="subtitle has-text-grey">Upload Image</p>
                      <ImageUpload onImageSelect={this._onPostImageSelect} />
                    </div>
                    <div className="column" />
                  </div>

                  <Button
                    loading={this.state.loading}
                    label={String.submit}
                    onClick={event => {
                      this._updateImage(event);
                    }}
                  />
                </div>
              </div>
            </div>
          </div>
        </section>
      </section>
    );
  }
}

export default AddNewPost;
