import React, { Component } from "react";
import services from "../services";
import PostComment from "../components/PostComments/comments";
import CommentInput from "../components/PostComments/input";
import * as userUtil from "../utils/user.util";
import { Link } from "react-router-dom";

class PostDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: "",
      likes: "",
      comments: [],
      likeBy: [],
      liked: -1
      //   comments: [],
      //   refreshing: false,
      //   likes: this.props.navigation.state.params.post.totalLikes
    };
  }

  componentDidMount() {
    this._getPost();
    this._fetchComments();
    // this.props.location.state.item
  }

  _getPost = async postId => {
    try {
      const getData = await services.auth.getPostById(
        this.props.match.params.id
      );
      console.log("This is get Data of get post", getData.data.likedBy);
      const { _id } = userUtil.getUserInfo();
      let result = getData.data.likedBy.indexOf(_id);
      this.setState({
        data: getData.data,
        likes: getData.data.totalLikes,
        liked: result
      });
    } catch (error) {
      console.log(error);
    }
  };

  _fetchComments = async postId => {
    try {
      const getComments = await services.auth.getCommentByPost(
        this.props.match.params.id
      );
      console.log("This is the comment Object", getComments);
      this.setState({ comments: getComments.data });
      console.log("Comments State:", this.state.comments);
    } catch (error) {
      console.log(error);
    }
  };

  _likePost = async postId => {
    try {
      const getLikes = await services.auth.likePost(this.props.match.params.id);
      console.log("This is like Api Hit", getLikes.data);
      this.setState({
        likes: getLikes.data.totalLikes,
        likeBy: getLikes.data.likedBy
      });
      const { _id } = userUtil.getUserInfo();
      let result = this.state.likeBy.indexOf(_id);
      console.log("This is likeby result", result);
      this.setState({ liked: result });
    } catch (error) {
      console.log(error);
    }
  };

  _onSubmit = async text => {
    try {
      const getData = await services.auth.postComment({
        postId: this.props.match.params.id,
        text: text,
        date: (new Date() / 1000) * 1000
      });
      const listComments = await services.auth.getCommentByPost(
        this.props.match.params.id
      );
      this.setState({ comments: listComments.data });

      // this.setState({ comments: listComments.data }, () => {
      //   this.props.navigation.state.params.refreshList();
      // });
      // console.log("This is the comment Id", listComments);
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    const item = this.state.data;
    return (
      <section>
        <section className="hero is-info is-small">
          <div className="hero-body">
            <div className="container has-text-centered">
              <p className="title">Topic Post</p>
            </div>
          </div>
        </section>
        <section className="container vendor ">
          <div className="columns features">
            <div class="column is-10 is-offset-1">
              <div className="card is-shady">
                <div className="card-content">
                  <div className="content blog">
                    <p className="title has-text-centered">{item.title}</p>
                    <p>{item.text}</p>
                  </div>
                </div>
                <div className="card-image">
                  <figure className="image is-5by3">
                    <img
                      src={item.media}
                      // alt="Placeholder image"
                      // className="modal-button"
                      // data-target="modal-image2"
                    />
                  </figure>
                </div>
                <div className="columns is-full status">
                  <div className="column">
                    <span onClick={() => this._likePost()}>
                      {this.state.liked >= 0 ? (
                        <i className="icon is-large status fas fa-thumbs-up" />
                      ) : (
                        <i className="icon is-large status far fa-thumbs-up" />
                      )}
                      {this.state.likes}
                    </span>
                    {}
                    <i className="icon is-large status far fa-comment" />
                    {this.state.comments.length}
                  </div>
                </div>
                <div className="columns is-full">
                  <div className="column comment">
                    <CommentInput onSubmit={this._onSubmit} />
                  </div>
                </div>
                {this.state.comments.map((item, i) => (
                  <div className="columns is-full">
                    <div className="column comment">
                      <PostComment comment={item} key={i} />
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </section>
      </section>
    );
  }
}

export default PostDetails;
