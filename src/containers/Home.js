import React, { Component } from "react";
import { Grid, Row, Col, Image } from "react-bootstrap";
import MainBackground from "../images/main-background.jpg";
import "./style.css";
import "reactbulma";
import Search from "../components/Search";
import PremiumSlideshow from "../components/premiumslideshow";
import { fakeAuthCentralState } from "../Routes";

class Home extends Component {
  render() {
    return (
      <div>
        <section className="hero is-large has-bg-img">
          <div className="background-overlay" />
          <div className="hero-body">
            <div className="container has-text-centered">
              <h1 className="title">
                DEALS & <br /> DISCOUNT WITH <br /> STUDENT <b>HUB</b>
              </h1>
              <h2 className="subtitle">Over 20,000 discounts. Grab one now!</h2>
              <div className="columns is-mobile is-centered">
                <div className="column is-half is-offset-one-quarter">
                  {/* <Search /> */}
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* <section className="categories-section">
          <div className="container">
            <div className="columns is-mobile">
              <div className="column"></div>
              <div className="column">
                <PremiumSlideshow />
              </div>
            </div>
          </div>
        </section> */}
        {/* <section className="brands-section">
          <div className="container">
            <h1 className="title">Section</h1>
            <h2 className="subtitle">
              A simple container to divide your page into
              <strong>sections</strong>, like the one you're currently reading
            </h2>
          </div>
        </section> */}
      </div>
    );
  }
}

export default Home;
