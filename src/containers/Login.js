import React, { Component } from "react";
import { Link } from "react-router-dom";
import services, { updateHeaders } from "../services";
import { emailConstraint, passwordConstraint } from "../utils/constraints";

import * as authUtil from "../utils/auth.util";
import * as userUtil from "../utils/user.util";
import "./style.css";
import { String } from "../utils/strings";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Button from "../components/loaderbutton";
import { fakeAuthCentralState } from "../Routes";
var validate = require("validate.js");

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      loading: false,
      redirectToReferrer: false
    };
  }

  componentDidMount() {
    const loginStatus = authUtil.isLoggedIn();
    const token = authUtil.getToken();
    console.log("This is user token", token);
    updateHeaders(token);
    console.log("LoginToken>>>>", loginStatus);
    if (loginStatus) {
      console.log("Iam login bro!>>>", loginStatus);
    }
    // AsyncStorage.clear();
  }

  _handleLogin = async event => {
    var constraints = {
      email: emailConstraint,
      password: passwordConstraint
    };
    var validation = validate(
      { email: this.state.email, password: this.state.password },
      constraints,
      { format: "flat" }
    );
    //   console.log(validation);
    if (validation) {
      toast.error(validation[0], {
        position: "bottom-center",
        autoClose: 2000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
      // alert(validation[0]);
      return;
    }
    try {
      this.setState({ loading: true });
      const { data } = await services.auth.login(this.state);
      console.log("Okay-data:", data.token);
      updateHeaders(data.token);
      await authUtil.setToken(data.token);
      await userUtil.setUserInfo(data);
      console.log("This is the user info", data);
      this.setState({ loading: false, redirectToReferrer: true });
      toast.success("Login Successfull", {
        position: "bottom-center",
        autoClose: 2000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
    } catch (error) {
      this.setState({ loading: false });
      toast.error(error.response.data.message, {
        position: "bottom-center",
        autoClose: 2000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
      console.log(error.response);
    }
  };

  render() {
    const { from } = this.props.location.state || { from: { pathname: "/" } };
    console.log("This is from state", from);
    const { redirectToReferrer } = this.state;

    if (redirectToReferrer === true) {
      this.props.history.push(from);
    }
    return (
      <section className="hero is-success is-fullheight">
        <div className="hero-body">
          <div className="container has-text-centered">
            <div className="column is-4 is-offset-4">
              <h3 className="title has-text-grey">Welcome</h3>
              <p className="subtitle has-text-grey">Login to Continue</p>
              <div className="box">
                <div className="field">
                  <div className="control">
                    <input
                      onChange={event => {
                        this.setState({ email: event.target.value });
                      }}
                      className="input is-large"
                      type="email"
                      placeholder={String.email}
                    />
                  </div>
                </div>
                <div className="field">
                  <div className="control">
                    <input
                      onChange={event =>
                        this.setState({ password: event.target.value })
                      }
                      className="input is-large"
                      type="password"
                      placeholder={String.password}
                    />
                  </div>
                </div>

                <Button
                  loading={this.state.loading}
                  label={String.login}
                  onClick={event => {
                    this._handleLogin(event);
                  }}
                />
                <ToastContainer
                  position="top-center"
                  autoClose={2000}
                  hideProgressBar
                  newestOnTop={false}
                  closeOnClick
                  rtl={false}
                  pauseOnVisibilityChange
                  draggable
                  pauseOnHover
                />
                {/* Same as */}
                <ToastContainer />
              </div>
              <p className="has-text-grey">
                <Link to="/signup">Sign Up</Link> &nbsp;·&nbsp;
                <Link to="../">Forgot Password</Link> &nbsp;·&nbsp;
                <Link to="/support">Support</Link>
              </p>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Login;
