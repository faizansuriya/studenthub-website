import "@babel/polyfill";
import React from "react";
import ReactDOM from "react-dom";

import "sanitize.css/sanitize.css";

// Import root app
import App from "./Routes";

// Observe loading of Open Sans (to remove open sans, remove the <link> tag in
// the index.html file and this observer)
/*const openSansObserver = new FontFaceObserver("Open Sans", {});

// When Open Sans is loaded, add a font-family using Open Sans to the body
openSansObserver.load().then(() => {
  document.body.classList.add("fontLoaded");
});*/

// Create redux store with history
const initialState = {};
// const store = configureStore(initialState, history);
const MOUNT_NODE = document.getElementById("app");

/*const render = messages => {
  ReactDOM.render(
   
      <App />
   ,
    MOUNT_NODE
  );
};*/

/*const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);*/
export default App;
