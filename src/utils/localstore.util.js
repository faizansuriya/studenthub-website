const localStoreUtil = {
  // setter: (key) => {
  //   localStorage.setItem(key, JSON.stringify(data));
  // },
  // getter: (key) => {
  //   const data = localStorage.getItem(key)
  //   if (!data) return;

  //   return JSON.parse(data);
  // },
  store_data: (key, data) => {
    localStorage.setItem(key, JSON.stringify(data));
    return true;
  },

  get_data: key => {
    const item = localStorage.getItem(key);
    console.log("This is item", item);

    if (!item) return;

    return JSON.parse(item);
  },

  remove_data: key => {
    localStorage.removeItem(key);
    return true;
  },

  remove_all: () => {
    localStorage.clear();
    return true;
  }
};

export default localStoreUtil;
