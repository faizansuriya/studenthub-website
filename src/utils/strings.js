export const String = {
  login: "Login",
  email: "Your Email",
  password: "Your Password",
  login: "Login",
  firstName: "First Name",
  lastName: "Last Name",
  phoneNumber: "Phone Number",
  password: "Password",
  confirmPassword: "Confirm Password",
  signUp: "Create Account",
  verify: "Enter Verification Code",
  submit: "Submit",
  instituteName: "Enter Institute Name",
  revealCode: "Reveal Code",
  addForumTitle: "Add Forum Title",
  addForumDescription: "Add Forum Description",
  readMore: "Read More"
};
