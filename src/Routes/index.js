/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from "react";
import { Helmet } from "react-helmet";
import styled from "styled-components";
import {
  Switch,
  Route,
  BrowserRouter,
  Redirect,
  withRouter
} from "react-router-dom";

import Home from "../containers/Home";
import Login from "../containers/Login";
import Signup from "../containers/Signup";
import AboutUs from "../containers/AboutUs";
import Premium from "../containers/Premium";
import Support from "../containers/Support";
import Blogs from "../containers/Blogs";
import PostDetails from "../containers/PostDetails";
import AddNewPost from "../containers/Addnewpost";
import Contact from "../containers/Contact";
import CategoryPage from "../containers/categorypage";
import MainLayout from "../components/MainLayout";
import ProtectedPage from "../containers/protectedpage";
import * as authUtil from "../utils/auth.util";
import { updateHeaders } from "../services";

// import GlobalStyle from '../../global-styles';

// export const fakeAuthCentralState = {
//   isAuthenticated: false,

//   authenticate(callback) {
//     this.isAuthenticated = true;
//     setTimeout(callback, 500);
//   },
//   signout(callback) {
//     this.isAuthenticated = false;
//     setTimeout(callback, 500);
//   }
// };

const AppWrapper = styled.div`
  margin: 0 auto;
  display: flex;
  min-height: 100%;
  flex-direction: column;
`;

// const ProtectedRoute = ({ component: Component, ...rest }) => (
//   <Route
//     {...rest}
//     render={props =>
//       fakeAuthCentralState.isAuthenticated === true ? (
//         <Component {...props} />
//       ) : (
//         <Redirect
//           to={{
//             pathname: "/login",
//             state: { from: props.location }
//           }}
//         />
//       )
//     }
//   />
// );

// const AuthButton = withRouter(({ history }) =>
//   fakeAuthCentralState.isAuthenticated ? (
//     <p>
//       Welcome to this amazing content!{" "}
//       <button
//         onClick={() => {
//           fakeAuthCentralState.signout(() => history.push("/login"));
//         }}
//       >
//         Sign out
//       </button>
//     </p>
//   ) : (
//     <p>You are not logged in.</p>
//   )
// );

export default function App() {
  return (
    <BrowserRouter>
      <AppWrapper>
        <MainLayout>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/login" component={Login} />
            <Route path="/signup" component={Signup} />
            <Route path="/premium" component={Premium} />
            <Route path="/aboutus" component={AboutUs} />
            <Route path="/support" component={Support} />
            <Route path="/blogs" component={Blogs} />
            <Route path="/blog/:title/:id" component={PostDetails} />
            <Route path="/addnewpost" component={AddNewPost} />
            <Route path="/contact" component={Contact} />
            <Route path="/category/:name/:id" component={CategoryPage} />

            {/* <ProtectedRoute path="/protected" component={ProtectedPage} /> */}
          </Switch>

          {/* <Route path="" component={NotFoundPage} />  */}
        </MainLayout>

        {/* <GlobalStyle /> */}
      </AppWrapper>
    </BrowserRouter>
  );
}
