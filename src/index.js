import "bootstrap/dist/css/bootstrap.min.css";
import $ from "jquery";
import Popper from "popper.js";
import "bootstrap/dist/js/bootstrap.bundle.min";
import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import "react-mdl/extra/material.css";
import "react-mdl/extra/material.js";
import { BrowserRouter } from "react-router-dom";
import App from "./Routes";

//const rootElement = document.getElementById("root");
//ReactDOM.render(<App />, rootElement);

ReactDOM.render(<App />, document.getElementById("root"));
//registerServiceWorker();
