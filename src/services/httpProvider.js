import axios from "axios";
import { getToken } from "../utils/auth.util";

const API_VER = "v1";
const BASE_URL = `https://studenthub-dev.herokuapp.com/api/${API_VER}`;

export function GetApiRequestHeader(token) {
  console.log("request header token==>", token);
  const authToken = token || (getToken());
  console.log("This is a auth token", authToken);
  return {
    Accept: "application/json",
    "Content-Type": "application/json",
    "x-access-token": authToken,
    Authorization: `Bearer ${authToken}`
  };
  if (authToken) {
    this.updateHeaders(authToken);
  }
}

const instance = axios.create({
  baseURL: `${BASE_URL}`,
  timeout: 60000,
  withCredentials: false,
  headers: GetApiRequestHeader()
});

export function updateHeaders(token) {
  // Alter defaults after instance has been created
  console.log("Update Header ==>", token);
  const header = GetApiRequestHeader(token);
  console.log("This is header", header);
  instance.defaults.headers = header;
}

export function request({ method, url, data, headers }) {
  console.log("method", method);
  console.log("url", url);
  console.log("data", data);
  console.log("header", headers);
  console.log(`Sending ${method} request to ${BASE_URL}`, url);

  var promise = instance[method](url, data);
  // console.log("Promise", promise);

  const response = promise;

  // console.log("request>>>", response);
  console.log(`Response from ${url}`, response);
  const payload = response;

  if (headers) {
    // return {
    //   data: payload,
    //   headers: response.headers
    // };
  }
  

  return payload;
}

export async function get(url, params, config) {
  console.log("Categories Url===>", url);
  return request({ method: "get", url, data: { params }, ...config });
}

export async function del(url, params, config) {
  return request({ method: "delete", url, data: { params }, ...config });
}

export async function post(url, data, config) {
  console.log("Url", url);
  console.log("data==>", data);
  // console.log("config", ...config);
  return request({ method: "post", url, data,...config });
}

export async function put(url, data, config) {
  return request({ method: "put", url, data, ...config });
}

// async function GetApiRequestHeader() {
//   debugger;
//   const authToken = await getToken();
//   return {
//     Accept: "application/json",
//     "Content-Type": "application/json",
//     Authorization: `Bearer ${authToken}`
//   };
// }

// _checkStatus(){

// }

// export function get({ url }) {
//   return axios.get(`${BASE_URL}/${url}`);
// }

// export async function post({ url, body }) {
// const _hToken ='eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1YmNmYTUxMThiMjk4NjQ1NWNiYzQ1ZWEiLCJlbWFpbCI6ImFzaWZAdGVzdC5jb20iLCJwYXNzd29yZCI6ImNmYTNkYWIwNmJkMzRlZDZkNTgzNWQ2MTYxY2IzMDQwIiwibW9iaWxlIjoiMTIzNDU3MDAiLCJpYXQiOjE1NDA4OTk0MTB9.ic3kY3NG54kFvTXwvOSvFSSqbDoiNjvqZMQEYyG6zZ4'
//   const token = await getToken();
//   console.log("This is my token", token);
//   const headers = {
//     "Content-Type": "application/json",
//     Authorization: `Bearer ${token}`
//   };
//   try {
//     return axios.post(`${BASE_URL}/${url}`, body, { headers });
//   } catch (error) {
//     console.log(error);
//   }
// }

// export function put({ url, body }) {
//   return axios.put(`${BASE_URL}/${url}`, body);
// }

// export function remove({ url }) {
//   return axios.remove(`${BASE_URL}/${url}`);
// }
