import { post, get, put, remove } from "./httpProvider";
import _ from "lodash";

const SERVICE_URLS = _.mapValues(
  {
    login: "/users/login",
    register: "users/signup",
    verification: "users/verify",
    institute: "users/updateinstitute",
    categories: "categories/get",
    revealCode: "code/reveal",
    getPremiumVendors: "vendors/premium",
    getAllPost: "forum/all",
    postComment: "comments/add",
    uploadImage: "vendors/upload",
    addForum: "forum/add"
  },
  _.template
);

const login = ({ email, password }) =>
  post(SERVICE_URLS.login(), { email, password });

const registerUser = ({ firstname, lastname, email, mobile, password }) =>
  post(SERVICE_URLS.register(), {
    firstname,
    lastname,
    email,
    mobile,
    password
  });

const getAllPost = () => get(SERVICE_URLS.getAllPost());

const verifyCode = ({ smsCode }) =>
  post(SERVICE_URLS.verification(), { smsCode });

const updateInstitute = body =>
  post(SERVICE_URLS.institute(), body, {
    headers: { "Content-Type": "multipart/form-data" }
  });

const getCategories = () => get(SERVICE_URLS.categories());
const revealCode = body => post(SERVICE_URLS.revealCode(), body);
const getPremiumVendors = () => get(SERVICE_URLS.getPremiumVendors());
const getVendorsByCategory = categoryId => {
  console.log("categoryId:");
  return get("vendors/getbycategory/" + categoryId);
};

const getPostById = postId => {
  return get("forum/get/" + postId);
};

const getCommentByPost = postId => {
  return get("comments/get/" + postId);
};

const likePost = postId => {
  return post("forum/" + postId + "/like");
};

const postComment = ({ postId, text, date }) =>
  post(SERVICE_URLS.postComment(), { post: postId, text, date });

const getImageUrl = body =>
  post(SERVICE_URLS.uploadImage(), body, {
    headers: { "Content-Type": "multipart/form-data" }
  });

const addPost = body => post(SERVICE_URLS.addForum(), body);

const authServices = {
  login,
  getCategories,
  registerUser,
  verifyCode,
  updateInstitute,
  getVendorsByCategory,
  revealCode,
  getPremiumVendors,
  getAllPost,
  getPostById,
  getCommentByPost,
  likePost,
  postComment,
  getImageUrl,
  addPost
};

export default authServices;
